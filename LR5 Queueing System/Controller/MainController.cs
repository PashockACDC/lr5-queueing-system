﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LR4_Queueing_System.Model;
using LR5_Queueing_System.Model;
using LR5_Queueing_System.ViewInterfaces;
using System.Threading;

namespace LR5_Queueing_System.Controller
{
    /// <summary>
    /// Главный контроллер
    /// </summary>
    class MainController
    {
        private IMainView mForm;
        private OuterGenerator mOuter;
        private ProcessingDevice mDevice;
        private StatisticsComputers mStatComputer;


        public MainController(IMainView form)
        {
            this.mForm = form;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="av"></param>
        /// <param name="scatter"></param>
        public void createSystem(int av, int scatter)
        {
            mOuter = new OuterGenerator(av, scatter);
            mOuter.onGenerating += MOuter_onGenerating;
            mOuter.onEndGenerating += MOuter_onEndGenerating;
            mDevice = new ProcessingDevice(mOuter);
            mDevice.onRequestAccepted += MDevice_onRequestAccepted;
            mDevice.onRequestCompleted += MDevice_onRequestCompleted;
            mDevice.onRequestFailed += MDevice_onRequestFailed;
            mStatComputer = new StatisticsComputers(mDevice);
        }

        public void onDestroy()
        {
            if (mOuter != null)
            {
                mOuter.onGenerating -= MOuter_onGenerating;
                mOuter.onEndGenerating -= MOuter_onEndGenerating;
                mOuter.onDestroy();
            }
            if (mDevice != null)
            {
                mDevice.onRequestAccepted -= MDevice_onRequestAccepted;
                mDevice.onRequestCompleted -= MDevice_onRequestCompleted;
                mDevice.onRequestFailed -= MDevice_onRequestFailed;
            }
            
        }

        /// <summary>
        /// Поступила заявка
        /// </summary>
        /// <param name="n">номер</param>
        /// <param name="t">время</param>
        private void MOuter_onGenerating(int n, double t)
        {
            int m = (int)t;
            int s = (int)((t - m) * 60);
            mForm.addMessageToOuterLog(n.ToString() + ": " + m + "m " + s + "s");
        }

        /// <summary>
        /// Окончание генерации 300 заявок
        /// </summary>
        private void MOuter_onEndGenerating()
        {
            mForm.showEndGenerating();
            mForm.showStatistic(mStatComputer.ToString(1, 2), 1);
            mForm.showStatistic(mStatComputer.ToString(3), 2);
        }

        /// <summary>
        /// Показываем сообщение, что заявка взята в обработку
        /// </summary>
        /// <param name="n">номер оператора</param>
        /// <param name="N">номер заявки</param>
        private void MDevice_onRequestAccepted(int n, int N)
        {
            mForm.addMessageToDeviceLog("№ " + N + " ...", n);
        }

        /// <summary>
        /// Показываем сообщение, что заявка обработана
        /// </summary>
        /// <param name="n">номер оператора, который обработал заявку</param>
        /// <param name="N">номер обработанной заявки</param>
        /// <param name="t">затраченное время (мин.)</param>
        private void MDevice_onRequestCompleted(int n, int N, double t)
        {
            int m = (int)t;
            int s = (int)((t - m) * 60);
            mForm.addMessageToDeviceLog("№ " + N  + " ..." + m + "m " + s + "s", n);
            mForm.showStatistic(mStatComputer.ToString(1, 2), 1);
            mForm.showStatistic(mStatComputer.ToString(3), 2);
        }

        /// <summary>
        /// Показываем номер пропавшей заявки
        /// </summary>
        /// <param name="N"></param>
        private void MDevice_onRequestFailed(int N)
        {
            mForm.showFailedRequest("Пропала заявка № " + N);
        }
    }
}
