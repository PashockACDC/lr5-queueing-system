﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LR5_Queueing_System.ViewInterfaces
{
    /// <summary>
    /// Соглашение о том, какие методы должно реализовать
    /// главное представление
    /// </summary>
    interface IMainView
    {
        /// <summary>
        /// Логирование заявок, приходящих с внешнего устройства
        /// </summary>
        /// <param name="message"></param>
        void addMessageToOuterLog(string message);

        /// <summary>
        /// Логирование для отдельных операторов
        /// </summary>
        /// <param name="message"></param>
        /// <param name="n">номер оператора</param>
        void addMessageToDeviceLog(string message, int n);

        /// <summary>
        /// (как-то) показать, что расчёт закончен
        /// </summary>
        void showEndGenerating();

        /// <summary>
        /// (как-то) показать пропавшую заявку
        /// </summary>
        /// <param name="message"></param>
        void showFailedRequest(string message);

        /// <summary>
        /// Показать статистику после окончания работы
        /// <param name="message"></param>
        /// <param name="n">для какого компьютера показать</param>
        /// </summary>
        void showStatistic(string message, int n);
    }
}
