﻿using LR5_Queueing_System.Controller;
using LR5_Queueing_System.Model;
using LR5_Queueing_System.ViewInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LR5_Queueing_System
{
    public partial class MainForm : Form, IMainView
    {
        private MainController mController;

        public MainForm()
        {
            InitializeComponent();
            this.mController = new MainController(this);
            this.label_version.Text = Constants.version;
            (new ToolTip()).SetToolTip(label_version, "Старцев П. С. группа ВИУ7-71");
        }

        /// <summary>
        /// Логирование заявок, приходящих с внешнего устройства
        /// </summary>
        /// <param name="message"></param>
        public void addMessageToOuterLog(string message)
        {
            try
            {
                this.Invoke((Action)(() =>
                {
                    StringBuilder str = new StringBuilder(textBox_log_outer.Text);
                    str.Append(message).Append("\r\n");
                    textBox_log_outer.Text = str.ToString();
                    textBox_log_outer.SelectionStart = textBox_log_outer.TextLength;
                    textBox_log_outer.ScrollToCaret();
                }));
            }
            catch (Exception ex)
            { /* не знаю как иначе обработать ошибку "Доступ к ликвидированному объекту невозможен" */ }
        }

        /// <summary>
        /// Логирование для отдельных операторов
        /// </summary>
        /// <param name="message"></param>
        /// <param name="n">номер оператора</param>
        public void addMessageToDeviceLog(string message, int n)
        {
            try
            {
                this.Invoke((Action)(() =>
                {
                    TextBox txtbox;
                    if (n == 1) txtbox = textBox_log_device_1;
                    else if (n == 2) txtbox = textBox_log_device_2;
                    else /*if (n == 3)*/ txtbox = textBox_log_device_3;
                    StringBuilder str = new StringBuilder(txtbox.Text);
                    str.Append(message).Append("\r\n");
                    txtbox.Text = str.ToString();
                    txtbox.SelectionStart = txtbox.TextLength;
                    txtbox.ScrollToCaret();
                }));
            }
            catch (Exception ex)
            { /* не знаю как иначе обработать ошибку "Доступ к ликвидированному объекту невозможен" */ }
        }

        /// <summary>
        /// Действия, показывающие, что генерация 300 заявок закончена
        /// </summary>
        public void showEndGenerating()
        {
            try
            {
                this.Invoke((Action)(() =>
                {
                    button_start.Enabled = true;
                    button_clear.Enabled = true;
                }));
            }
            catch (Exception ex)
            { /* не знаю как иначе обработать ошибку "Доступ к ликвидированному объекту невозможен" */ }
        }

        /// <summary>
        /// Покаываем номер пропавшей заявки
        /// </summary>
        /// <param name="message"></param>
        public void showFailedRequest(string message)
        {
            try
            {
                this.Invoke((Action)(() =>
                {
                    StringBuilder str = new StringBuilder(textBox_log_failed.Text);
                    str.Append(message).Append("\r\n");
                    textBox_log_failed.Text = str.ToString();
                    textBox_log_failed.SelectionStart = textBox_log_failed.TextLength;
                    textBox_log_failed.ScrollToCaret();
                }));
            }
            catch (Exception ex)
            { /* не знаю как иначе обработать ошибку "Доступ к ликвидированному объекту невозможен" */ }
        }

        /// <summary>
        /// Показать статистику после окончания работы
        /// </summary>
        /// <param name="message"></param>
        /// <param name="n">для какого компьютера показать</param>
        public void showStatistic(string message, int n)
        {
            try
            {
                this.Invoke((Action)(() =>
                {
                    TextBox txtbox;
                    if (n == 1)
                    {
                        txtbox = textBox_stat_1_2;
                    }
                    else // if (n == 2)
                    {
                        txtbox = textBox_stat_3;
                    }
                    txtbox.Text = message;
                    txtbox.SelectionStart = txtbox.TextLength;
                    txtbox.ScrollToCaret();
                }));
            }
            catch (Exception ex)
            { /* не знаю как иначе обработать ошибку "Доступ к ликвидированному объекту невозможен" */ }
        }

        private void button_start_Click(object sender, EventArgs e)
        {
            mController.createSystem((int)numUD_out_average.Value,
                                     (int)numUD_out_scatter.Value);
            button_start.Enabled = false;
            button_clear.Enabled = false;
        }

        private void button_clear_Click(object sender, EventArgs e)
        {
            textBox_log_outer.Text = "";
            textBox_log_device_1.Text = "";
            textBox_log_device_2.Text = "";
            textBox_log_device_3.Text = "";
            textBox_log_failed.Text = "";
            textBox_stat_1_2.Text = "";
            textBox_stat_3.Text = "";
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            mController.onDestroy();
        }

    }
}
