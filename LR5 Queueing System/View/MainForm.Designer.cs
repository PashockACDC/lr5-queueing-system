﻿namespace LR5_Queueing_System
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_version = new System.Windows.Forms.Label();
            this.textBox_log_outer = new System.Windows.Forms.TextBox();
            this.groupBox_OuterRequests = new System.Windows.Forms.GroupBox();
            this.groupBox_device_1 = new System.Windows.Forms.GroupBox();
            this.textBox_log_device_1 = new System.Windows.Forms.TextBox();
            this.groupBox_device_2 = new System.Windows.Forms.GroupBox();
            this.textBox_log_device_2 = new System.Windows.Forms.TextBox();
            this.groupBox_device_3 = new System.Windows.Forms.GroupBox();
            this.textBox_log_device_3 = new System.Windows.Forms.TextBox();
            this.button_start = new System.Windows.Forms.Button();
            this.button_clear = new System.Windows.Forms.Button();
            this.groupBox_failed = new System.Windows.Forms.GroupBox();
            this.textBox_log_failed = new System.Windows.Forms.TextBox();
            this.groupBox_stat_1_2 = new System.Windows.Forms.GroupBox();
            this.textBox_stat_1_2 = new System.Windows.Forms.TextBox();
            this.groupBox_stat_3 = new System.Windows.Forms.GroupBox();
            this.textBox_stat_3 = new System.Windows.Forms.TextBox();
            this.numUD_out_average = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.numUD_out_scatter = new System.Windows.Forms.NumericUpDown();
            this.groupBox_OuterRequests.SuspendLayout();
            this.groupBox_device_1.SuspendLayout();
            this.groupBox_device_2.SuspendLayout();
            this.groupBox_device_3.SuspendLayout();
            this.groupBox_failed.SuspendLayout();
            this.groupBox_stat_1_2.SuspendLayout();
            this.groupBox_stat_3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_out_average)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_out_scatter)).BeginInit();
            this.SuspendLayout();
            // 
            // label_version
            // 
            this.label_version.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_version.AutoSize = true;
            this.label_version.Font = new System.Drawing.Font("Consolas", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_version.ForeColor = System.Drawing.Color.DarkSeaGreen;
            this.label_version.Location = new System.Drawing.Point(732, 1);
            this.label_version.Name = "label_version";
            this.label_version.Size = new System.Drawing.Size(37, 13);
            this.label_version.TabIndex = 0;
            this.label_version.Text = "v.#.#";
            // 
            // textBox_log_outer
            // 
            this.textBox_log_outer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_log_outer.Location = new System.Drawing.Point(4, 40);
            this.textBox_log_outer.Multiline = true;
            this.textBox_log_outer.Name = "textBox_log_outer";
            this.textBox_log_outer.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_log_outer.Size = new System.Drawing.Size(96, 470);
            this.textBox_log_outer.TabIndex = 1;
            // 
            // groupBox_OuterRequests
            // 
            this.groupBox_OuterRequests.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox_OuterRequests.Controls.Add(this.numUD_out_scatter);
            this.groupBox_OuterRequests.Controls.Add(this.label1);
            this.groupBox_OuterRequests.Controls.Add(this.numUD_out_average);
            this.groupBox_OuterRequests.Controls.Add(this.textBox_log_outer);
            this.groupBox_OuterRequests.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox_OuterRequests.Location = new System.Drawing.Point(2, 1);
            this.groupBox_OuterRequests.Name = "groupBox_OuterRequests";
            this.groupBox_OuterRequests.Size = new System.Drawing.Size(104, 514);
            this.groupBox_OuterRequests.TabIndex = 2;
            this.groupBox_OuterRequests.TabStop = false;
            this.groupBox_OuterRequests.Text = "Заявки";
            // 
            // groupBox_device_1
            // 
            this.groupBox_device_1.Controls.Add(this.textBox_log_device_1);
            this.groupBox_device_1.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox_device_1.Location = new System.Drawing.Point(112, 1);
            this.groupBox_device_1.Name = "groupBox_device_1";
            this.groupBox_device_1.Size = new System.Drawing.Size(130, 170);
            this.groupBox_device_1.TabIndex = 3;
            this.groupBox_device_1.TabStop = false;
            this.groupBox_device_1.Text = "Оператор № 1";
            // 
            // textBox_log_device_1
            // 
            this.textBox_log_device_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_log_device_1.Location = new System.Drawing.Point(4, 15);
            this.textBox_log_device_1.Multiline = true;
            this.textBox_log_device_1.Name = "textBox_log_device_1";
            this.textBox_log_device_1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_log_device_1.Size = new System.Drawing.Size(122, 151);
            this.textBox_log_device_1.TabIndex = 1;
            // 
            // groupBox_device_2
            // 
            this.groupBox_device_2.Controls.Add(this.textBox_log_device_2);
            this.groupBox_device_2.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox_device_2.Location = new System.Drawing.Point(112, 173);
            this.groupBox_device_2.Name = "groupBox_device_2";
            this.groupBox_device_2.Size = new System.Drawing.Size(130, 170);
            this.groupBox_device_2.TabIndex = 4;
            this.groupBox_device_2.TabStop = false;
            this.groupBox_device_2.Text = "Оператор № 2";
            // 
            // textBox_log_device_2
            // 
            this.textBox_log_device_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_log_device_2.Location = new System.Drawing.Point(4, 15);
            this.textBox_log_device_2.Multiline = true;
            this.textBox_log_device_2.Name = "textBox_log_device_2";
            this.textBox_log_device_2.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_log_device_2.Size = new System.Drawing.Size(122, 151);
            this.textBox_log_device_2.TabIndex = 1;
            // 
            // groupBox_device_3
            // 
            this.groupBox_device_3.Controls.Add(this.textBox_log_device_3);
            this.groupBox_device_3.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox_device_3.Location = new System.Drawing.Point(112, 345);
            this.groupBox_device_3.Name = "groupBox_device_3";
            this.groupBox_device_3.Size = new System.Drawing.Size(130, 170);
            this.groupBox_device_3.TabIndex = 5;
            this.groupBox_device_3.TabStop = false;
            this.groupBox_device_3.Text = "Оператор № 3";
            // 
            // textBox_log_device_3
            // 
            this.textBox_log_device_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_log_device_3.Location = new System.Drawing.Point(4, 15);
            this.textBox_log_device_3.Multiline = true;
            this.textBox_log_device_3.Name = "textBox_log_device_3";
            this.textBox_log_device_3.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_log_device_3.Size = new System.Drawing.Size(122, 151);
            this.textBox_log_device_3.TabIndex = 1;
            // 
            // button_start
            // 
            this.button_start.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_start.Image = global::LR5_Queueing_System.Properties.Resources.Start;
            this.button_start.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_start.Location = new System.Drawing.Point(683, 17);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(74, 30);
            this.button_start.TabIndex = 6;
            this.button_start.Text = "Старт !!!";
            this.button_start.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button_start.UseVisualStyleBackColor = true;
            this.button_start.Click += new System.EventHandler(this.button_start_Click);
            // 
            // button_clear
            // 
            this.button_clear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_clear.ForeColor = System.Drawing.Color.DarkRed;
            this.button_clear.Location = new System.Drawing.Point(683, 53);
            this.button_clear.Name = "button_clear";
            this.button_clear.Size = new System.Drawing.Size(75, 23);
            this.button_clear.TabIndex = 7;
            this.button_clear.Text = "Очистить";
            this.button_clear.UseVisualStyleBackColor = true;
            this.button_clear.Click += new System.EventHandler(this.button_clear_Click);
            // 
            // groupBox_failed
            // 
            this.groupBox_failed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox_failed.Controls.Add(this.textBox_log_failed);
            this.groupBox_failed.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox_failed.Location = new System.Drawing.Point(248, 1);
            this.groupBox_failed.Name = "groupBox_failed";
            this.groupBox_failed.Size = new System.Drawing.Size(169, 514);
            this.groupBox_failed.TabIndex = 3;
            this.groupBox_failed.TabStop = false;
            this.groupBox_failed.Text = "Пропавшие заявки";
            // 
            // textBox_log_failed
            // 
            this.textBox_log_failed.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_log_failed.Location = new System.Drawing.Point(4, 15);
            this.textBox_log_failed.Multiline = true;
            this.textBox_log_failed.Name = "textBox_log_failed";
            this.textBox_log_failed.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_log_failed.Size = new System.Drawing.Size(161, 495);
            this.textBox_log_failed.TabIndex = 1;
            // 
            // groupBox_stat_1_2
            // 
            this.groupBox_stat_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox_stat_1_2.Controls.Add(this.textBox_stat_1_2);
            this.groupBox_stat_1_2.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox_stat_1_2.Location = new System.Drawing.Point(423, 1);
            this.groupBox_stat_1_2.Name = "groupBox_stat_1_2";
            this.groupBox_stat_1_2.Size = new System.Drawing.Size(176, 254);
            this.groupBox_stat_1_2.TabIndex = 4;
            this.groupBox_stat_1_2.TabStop = false;
            this.groupBox_stat_1_2.Text = "Статистика по принятым (1, 2)";
            // 
            // textBox_stat_1_2
            // 
            this.textBox_stat_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_stat_1_2.Location = new System.Drawing.Point(4, 15);
            this.textBox_stat_1_2.Multiline = true;
            this.textBox_stat_1_2.Name = "textBox_stat_1_2";
            this.textBox_stat_1_2.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_stat_1_2.Size = new System.Drawing.Size(168, 235);
            this.textBox_stat_1_2.TabIndex = 1;
            // 
            // groupBox_stat_3
            // 
            this.groupBox_stat_3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox_stat_3.Controls.Add(this.textBox_stat_3);
            this.groupBox_stat_3.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox_stat_3.Location = new System.Drawing.Point(423, 255);
            this.groupBox_stat_3.Name = "groupBox_stat_3";
            this.groupBox_stat_3.Size = new System.Drawing.Size(176, 260);
            this.groupBox_stat_3.TabIndex = 5;
            this.groupBox_stat_3.TabStop = false;
            this.groupBox_stat_3.Text = "Статистика по принятым (3)";
            // 
            // textBox_stat_3
            // 
            this.textBox_stat_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_stat_3.Location = new System.Drawing.Point(4, 15);
            this.textBox_stat_3.Multiline = true;
            this.textBox_stat_3.Name = "textBox_stat_3";
            this.textBox_stat_3.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_stat_3.Size = new System.Drawing.Size(168, 241);
            this.textBox_stat_3.TabIndex = 1;
            // 
            // numUD_out_average
            // 
            this.numUD_out_average.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numUD_out_average.Location = new System.Drawing.Point(10, 14);
            this.numUD_out_average.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numUD_out_average.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numUD_out_average.Name = "numUD_out_average";
            this.numUD_out_average.Size = new System.Drawing.Size(36, 20);
            this.numUD_out_average.TabIndex = 2;
            this.numUD_out_average.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(48, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "±";
            // 
            // numUD_out_scatter
            // 
            this.numUD_out_scatter.Location = new System.Drawing.Point(62, 14);
            this.numUD_out_scatter.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numUD_out_scatter.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUD_out_scatter.Name = "numUD_out_scatter";
            this.numUD_out_scatter.Size = new System.Drawing.Size(36, 20);
            this.numUD_out_scatter.TabIndex = 4;
            this.numUD_out_scatter.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(769, 517);
            this.Controls.Add(this.groupBox_stat_3);
            this.Controls.Add(this.groupBox_stat_1_2);
            this.Controls.Add(this.groupBox_failed);
            this.Controls.Add(this.button_clear);
            this.Controls.Add(this.button_start);
            this.Controls.Add(this.groupBox_device_3);
            this.Controls.Add(this.groupBox_device_2);
            this.Controls.Add(this.groupBox_device_1);
            this.Controls.Add(this.groupBox_OuterRequests);
            this.Controls.Add(this.label_version);
            this.MinimumSize = new System.Drawing.Size(708, 555);
            this.Name = "MainForm";
            this.Text = "LR5 Queueing System";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.groupBox_OuterRequests.ResumeLayout(false);
            this.groupBox_OuterRequests.PerformLayout();
            this.groupBox_device_1.ResumeLayout(false);
            this.groupBox_device_1.PerformLayout();
            this.groupBox_device_2.ResumeLayout(false);
            this.groupBox_device_2.PerformLayout();
            this.groupBox_device_3.ResumeLayout(false);
            this.groupBox_device_3.PerformLayout();
            this.groupBox_failed.ResumeLayout(false);
            this.groupBox_failed.PerformLayout();
            this.groupBox_stat_1_2.ResumeLayout(false);
            this.groupBox_stat_1_2.PerformLayout();
            this.groupBox_stat_3.ResumeLayout(false);
            this.groupBox_stat_3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_out_average)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_out_scatter)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_version;
        private System.Windows.Forms.TextBox textBox_log_outer;
        private System.Windows.Forms.GroupBox groupBox_OuterRequests;
        private System.Windows.Forms.GroupBox groupBox_device_1;
        private System.Windows.Forms.TextBox textBox_log_device_1;
        private System.Windows.Forms.GroupBox groupBox_device_2;
        private System.Windows.Forms.TextBox textBox_log_device_2;
        private System.Windows.Forms.GroupBox groupBox_device_3;
        private System.Windows.Forms.TextBox textBox_log_device_3;
        private System.Windows.Forms.Button button_start;
        private System.Windows.Forms.Button button_clear;
        private System.Windows.Forms.GroupBox groupBox_failed;
        private System.Windows.Forms.TextBox textBox_log_failed;
        private System.Windows.Forms.GroupBox groupBox_stat_1_2;
        private System.Windows.Forms.TextBox textBox_stat_1_2;
        private System.Windows.Forms.GroupBox groupBox_stat_3;
        private System.Windows.Forms.TextBox textBox_stat_3;
        private System.Windows.Forms.NumericUpDown numUD_out_scatter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numUD_out_average;
    }
}

