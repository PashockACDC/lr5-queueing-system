﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LR5_Queueing_System.Model
{
    public static class Constants
    {
        public const string version = "v.0.5";

        /// <summary>коэффициент масштабирования времени</summary>
        public const int TIME_SCALE = 10000;

    }
}
