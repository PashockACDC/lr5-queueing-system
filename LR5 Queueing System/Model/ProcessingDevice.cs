﻿using LR4_Queueing_System.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LR5_Queueing_System.Model
{
    /// <summary>
    /// <para>Обслуживающее устройство.</para>
    /// <para>Имеет 3 оператора</para>
    /// </summary>
    class ProcessingDevice
    {

        /// <summary>количество операторов</summary>
        private int mN;
        /// <summary>номер текущей обрабатываемой заявки</summary>
        private int mNCurrentRequest;
        /// <summary>минимальное время (сек.) обслуживания заявки каждым оператором</summary>
        private double[] mMinTimeProcessing;
        /// <summary>максимальное время (сек.) обслуживания заявки каждым оператором</summary>
        private double[] mMaxTimeProcessing;

        /// <summary>поток, отслеживающий всю статистику</summary>
        private Thread mThreadStatistics;
        /// <summary>поток, обрабатывающий поступление внешних событий (заявок)</summary>
        private Thread mThreadCommonProcessing;
        /// <summary>пул потоков для каждого оператора</summary>
        private Thread[] mThreadsForOperators;
        /// <summary>флаги - обрабатываются ли заявки операторами</summary>
        private bool[] isOperatorsBusy;

        /// <summary>существует ли в данный момент обслуживающее устройство</summary>
        private bool isExist = false;

        /// <summary>Ссылка на внешний генератор</summary>
        private OuterGenerator mOuter;

        /// <summary>Событие - заявка принята:
        /// <para>- номер оператора, который будет обрабатывать заявку</para>
        /// <para>- номер обрабатываемой заявки</para>
        /// </summary>
        public event Action<int, int> onRequestAccepted;
        /// <summary>Событие - заявка обработана:
        /// <para>- номер оператора, который обработал заявку (начиная с 1)</para>
        /// <para>- номер обработанной заявки</para>
        /// <para>- затраченное время (мин.)</para>
        /// </summary>
        public event Action<int, int, double> onRequestCompleted;
        /// <summary>Событие - заявка пропала
        /// <para>- номер пропавшей заявки</para>
        /// </summary>
        public event Action<int> onRequestFailed;


        public ProcessingDevice(OuterGenerator outer)
        {
            mOuter = outer;
            mOuter.onGenerating += running;
            mN = 3;
            mMinTimeProcessing = new double[mN];
            mMinTimeProcessing[0] = (double)(20 - 5) * 60 / Constants.TIME_SCALE;
            mMinTimeProcessing[1] = (double)(40 - 10) * 60 / Constants.TIME_SCALE;
            mMinTimeProcessing[2] = (double)(40 - 20) * 60 / Constants.TIME_SCALE;
            mMaxTimeProcessing = new double[mN];
            mMaxTimeProcessing[0] = (double)(20 + 5) * 60 / Constants.TIME_SCALE;
            mMaxTimeProcessing[1] = (double)(40 + 10) * 60 / Constants.TIME_SCALE;
            mMaxTimeProcessing[2] = (double)(40 + 20) * 60 / Constants.TIME_SCALE;
            isOperatorsBusy = new bool[mN];
            mThreadsForOperators = new Thread[mN];
        }

        /// <summary>
        /// Основной обрабатывающий метод
        /// </summary>
        private void running(int N, double d)
        {
            if (!isOperatorsBusy[0]) // если свободен первый оператор
            {
                mThreadsForOperators[0] = new Thread( () => {
                    runningOperators(0, N);
                });
                mThreadsForOperators[0].Start();
            }
            else if (!isOperatorsBusy[1]) // если свободен второй оператор
            {
                mThreadsForOperators[1] = new Thread( () => {
                    runningOperators(1, N);
                });
                mThreadsForOperators[1].Start();
            }
            else if (!isOperatorsBusy[2]) // если свободен третий оператор
            {
                mThreadsForOperators[2] = new Thread( () => {
                    runningOperators(2, N);
                });
                mThreadsForOperators[2].Start();
            }
            else // все заняты - потеря заявки
            {
                if (onRequestFailed != null)
                    onRequestFailed(N);
            }
        }

        /// <summary>
        /// Обрабатывающий делегат для каждого оператора
        /// <param name="n">номер оператора (отсчёт с нуля)</param>
        /// <param name="N">номер обрабатываемой заявки</param>
        /// </summary>
        private void runningOperators(int n, int N)
        {
            isOperatorsBusy[n] = true;
            if (onRequestAccepted != null)
                onRequestAccepted(n + 1, N);
            Random rand = new Random();
            double t = mMinTimeProcessing[n] +
                       rand.NextDouble() * (mMaxTimeProcessing[n] - mMinTimeProcessing[n]);
            Thread.Sleep((int)(t * 1000));
            if (onRequestCompleted != null)
                onRequestCompleted(n + 1, N, t / 60 * Constants.TIME_SCALE);
            isOperatorsBusy[n] = false;
        }
    }
}
