﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LR5_Queueing_System.Model
{
    /// <summary>
    /// Компьютер(ы) для сбора статистики (?)
    /// </summary>
    class StatisticsComputers
    {
        /// <summary>
        /// Обработанная заявка
        /// </summary>
        private struct CompletedRequest
        {
            /// <summary>номер оператора, который обработал заявку</summary>
            public int n;
            /// <summary>номер обработанной заявки</summary>
            public int N;
            /// <summary>затраченное время оператора (мин.)</summary>
            public double t;
        }
        private ProcessingDevice mDevices;
        private List<CompletedRequest> mListStatistic;


        public StatisticsComputers(ProcessingDevice device)
        {
            mListStatistic = new List<CompletedRequest>();
            mDevices = device;
            mDevices.onRequestCompleted += MDevices_onRequestCompleted;
        }


        /// <summary>
        /// Сбор статистики по обработанным заявкам
        /// </summary>
        /// <param name="n">номер оператора, который обработал заявку</param>
        /// <param name="N">номер обработанной заявки</param>
        /// <param name="t">затраченное время оператора (мин.)</param>
        private void MDevices_onRequestCompleted(int n, int N, double t)
        {
            CompletedRequest req = new CompletedRequest();
            req.n = n;
            req.N = N;
            req.t = t;
            mListStatistic.Add(req);
        }

        /// <summary>
        /// Текстовое представление статистики
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder res = new StringBuilder();
            foreach (CompletedRequest i in mListStatistic)
            {
                res.Append(i.n).Append(": ");
                res.Append(i.N).Append(" ... ");
                int m = (int)i.t;
                int s = (int)((i.t - m) * 60);
                res.Append(m).Append("m ").Append(s).Append("s").Append("\r\n");
            }
            return res.ToString();
        }

        /// <summary>
        /// Текстовое представление статистики
        /// </summary>
        /// <param name="nn">номера операторов, статистику по которым надо отобразить</param>
        /// <returns></returns>
        public string ToString(params int[] nn)
        {
            StringBuilder res = new StringBuilder();
            for (int i = 0; i < mListStatistic.Count; i++)
            //foreach (CompletedRequest i in mListStatistic)
            {
                if (nn.Contains(mListStatistic[i].n))
                {
                    res.Append(mListStatistic[i].n).Append(": ");
                    res.Append(mListStatistic[i].N).Append(" ... ");
                    int m = (int)mListStatistic[i].t;
                    int s = (int)((mListStatistic[i].t - m) * 60);
                    res.Append(m).Append("m ").Append(s).Append("s").Append("\r\n");
                }
            }
            return res.ToString();
        }
    }
}
