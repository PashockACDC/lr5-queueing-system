﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LR5_Queueing_System.Model;

namespace LR4_Queueing_System.Model
{
    /// <summary>
    /// Генератор внешних событий
    /// (по равномерному закону распределения)
    /// </summary>
    public class OuterGenerator
    {
        /// <summary>номер генерируемой завяки</summary>
        private int mN = 1;
        /// <summary>нижний предел (сек) времени поступления заявки</summary>
        private double minTime;
        /// <summary>верхний предел (сек) времени поступления заявки</summary>
        private double maxTime;

        /// <summary>поток, генерирующий внешние события (поступление заявки)</summary>
        private Thread threadOuterGenerator;
        /// <summary>генерируются ли события в данный момент</summary>
        private bool isGenerating = false;
        /// <summary>существует ли в данный момент внешний генератор</summary>
        private bool isExist = false;

        /// <summary>главное событие - поступила заявка
        /// <para>- номер поступившей заявки</para>
        /// <para>- интервал времени поступления заявки (мин.)</para></summary>
        public event Action<int, double> onGenerating;
        /// <summary>событие - генерация 300 заявок окончена</summary>
        public event Action onEndGenerating;

        /// <summary>
        /// Создание внешнего генератора событий
        /// с заданными параметрами
        /// </summary>
        /// <param name="avT">среднее время (мин.) поступления заявки</param>
        /// <param name="scatterT">разброс времени (мин.) поступления заявки</param>
        public OuterGenerator(double avT = 8, double scatterT = 12)
        {
            this.minTime = (avT - scatterT) * 60 / Constants.TIME_SCALE;
            this.maxTime = (avT + scatterT) * 60 / Constants.TIME_SCALE;
            this.isExist = true;
            this.isGenerating = true;
            this.threadOuterGenerator = new Thread(running);
            this.threadOuterGenerator.Start();
        }

        /// <summary>
        /// Уничтожение генератора внешней среды
        /// (новый создавать только через new !!!)
        /// </summary>
        public void onDestroy()
        {
            this.isExist = false;
        }


        /// <summary>
        /// Основной обрабатывающий поток
        /// </summary>
        private void running()
        {
            while (isExist)
            {
                if (isGenerating)
                {
                    Random rand = new Random();
                    double t = minTime + rand.NextDouble() * (maxTime - minTime);
                    Thread.Sleep((int)(t * 1000));
                    if (onGenerating != null)
                        onGenerating(mN++, t / 60 * Constants.TIME_SCALE);
                }
                if (mN > 300)
                {
                    if (onEndGenerating != null)
                        onEndGenerating();
                    break;
                }
            }
        }

    }
}
